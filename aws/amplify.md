# Amplify Commands

## Amplify gen 1
```bash
amplify init
# api can be of two types (rest and graphql)
amplify add api
# add storage (must be authenticated)
amplify add storage
## remove 
amplify remove {{api|storage}}
amplify push
amplify api gql-compile
# 
amplify push --allow-destructive-graphql-schema-updates
# rebuild all of the tables backing our schema
amplify rebuild api
```
## Amplify gen 2
https://www.npmjs.com/package/@aws-amplify/backend-cli
```bash
ampx sandbox --profile {{profile_name}}
```