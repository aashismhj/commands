# CDK Commands
https://docs.aws.amazon.com/cdk/v2/guide/tools.html 
```bash
## install cdk
yarn global add aws-cdk
## get version 
cdk --version
```

## app init
```bash
cdk init app --language {{language}} ## initialize for a cdk application
## available languages csharp, fsharp, go, java, javascript, python, typescript
cdk init lib --language typescript # initialize template for cdk construct library
cdk init sample-app --language {{language}} # init a example cdk application with some constructs

```

## CDK Setup
```bash
cdk ls # list all stacks in the app
cdk synth # print the synthesized CloudFormation template
cdk bootstrap # install stack resources that are needed for the toolkit's operation
```

## Deploy commands
```bash
cdk deploy --no-execute-changeset # create and display the changeset without executing it
cdk deploy # deploy this stack to our default AWS account/region
cdk deploy {{stack}} # if there are multiple stack and you want to run a specific stack
cdk deploy -all # run add stacks
cdk deploy --hotswap # assess whether a hotswap deployment can be performed instead of a CloudFormation deployment
cdk deploy --hotswap-fallback
cdk watch # inspect changes and deploy 
cdk watch --no-hotswap # by default watch uses hot swap this will disable it
cdk diff # compare deployed stack with current state
cdk diff --no-change-set # less accurate but faster template-only diff
cdk destroy # delete stack
```

## Main commands
```bash
cdk list # List all stack in the app
cdk docs # open cdk documentation
cdk import # Import existing resources into the given STACK
cdk metadata # Returns all metadata associated with the stack
cdk notices # Returns all list or relevant notices
cdk context # Manage cached context values
cdk doctor # Check setup for potential problems
```

## Options
```bash
cdk synth --json ## Use JSON output instead of YAML when templates are printed to STDOUT
cdk deploy --profile {{profile_name}} ## Use the indicated AWS profile as the default environment
cdk deploy --role-arn {{arn}} # ARN of Role to use when invoking CloudFormation
```