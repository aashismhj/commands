## AWS Commands
```bash
aws apigateway get-export --parameters extensions='apigateway' --rest-api-id {{api_id}} --stage-name {{stage_name}} --export-type swagger {{file_name}}.json
```