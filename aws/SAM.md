# SAM commands

```bash
sam init
## build application
sam build
sam validate
## invoke function 
sam local invoke
## host api locally
sam local start-api
## interact with function in the cloud
sam remote invoke {{function_name}} --stack-name <stack_name/>
## listing api endpoints
sam list endpoints --output json
## test function in the cloud
sam sync --stack-name {stack_name} --watch --region {{region}} 
## build application
sam build
```

## Deployment Commands
```bash
## deploy
sam deploy --guided
## deploy using a seperate env config
sam deploy --config-env {{env_name}}
sam deploy --config-file myconfig.yaml
```

## Pipeline
```bash
## create pipeline
sam pipeline bootstrap
sam pipeline init --bootstrap
# create pipeline configuration file
sam pipeline init
sam deploy -t codepipeline.yaml --stack-name pipeline-sam-app --capabilities=CAPABILITY_IAM
```

```bash
# Package the application
sam package --template-file template.yaml --output-template-file packaged.yaml --s3-bucket YOUR_S3_BUCKET_NAME

# Deploy the application
sam deploy --template-file packaged.yaml --stack-name your-sam-application --capabilities CAPABILITY_IAM

```

**Note**
- You must have the same version of language as specified in template.yaml
