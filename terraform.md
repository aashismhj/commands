# 

```bash
# install
choco install terraform
# check 
terraform --version

# 
terraform init

# check syntax
terraform validate

# 
terraform apply
```