# Fastify

```bash
# install commands
npm install -g fastify-cli
```

## start server
```bash
fastify start # start a server
fastify start -w -l info -P {{file_name}} # start server in watch mode with log
```

```bash
fastify eject # turns your application into a standalone
fastify generate # generate a new project
fastify generate-plugin # generate a new plugin project
fastify generate-swagger # generate Swagger/OpenAI schema for a project
fastify readme # generate README.md for the plugin
fastify print-routes # print the representation of the internal
fastify print-plugins # prints the representation of the internal
fastify version # print current version
fastify help {{command}} # help about command
```