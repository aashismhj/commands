# Docker compose commands

```bash
docker-compose build # build docker compose
docker-compose up # run 
docker-compose up --build # build and run
docker-compose down
```