# Docker Commands
https://docs.docker.com/reference/cli/docker/

## Build commands
```bash
docker build --build-arg SOME_ENV=my-env --progress plain -t my-tag -f {{docker_file_path}} {{location}}
```

## container commands
```bash
## container automatically removed when existed
docker run -rm 
docker ps # list running docker containers
docker ps -a # list all containers
docker stop {{container_name|container_id}} # stop container
docker rm {{container_name|container_id}} # container to delete container
docker rm -f {{container_name|container_id}} # forcefully remove even if its running
docker container prune -f # remove all stop containers
docker update {{update configuration}} {{container_name|container_id}} # update container
docker update --restart {{option}} {{container_name|container_id}} # update container restart policy
```



**Docker run options**
```bash
docker run --name {{name}}  -p {{port_mapping}} # run with container and port mapping
docker run --name {{name}} -d # run container with detached mode
docker run --network {{network_name}} # run container in a network
docker run --name {{name}} -v {{host/path:/container/path}} # run container with volume mapping
```

## network commands
```bash
docker network connect # connect a container to a network
docker network create # create a network
docker network disconnect # disconnect a container from a network
docker network inspect display # display detailed information on one or more networks
docker network ls # list networks
docker network prune # remove all unused networks
docker network rm # remove one or more networks
```