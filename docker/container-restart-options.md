# Restart Options

1. no
will not restart automatically, even if it exists or the docker daemon restarts
```bash
docker run --restart no {{image_name}}
```

2. always
will always restart automatically when the docker daemon starts, or if the container exits, regardless of the exit status
```bash
docker run --restart always {{image_name}}
```

3. on-failure[:max-retries]
container will restart only if it exits with a non-zero exists status, we can optionally specify a maximum number of retries before giving up
```bash
docker run --restart on-failure:5 {{image_name}}
```

4. unless-stopped
container will restart unless it was explicitly stopped by the user, it will not restart if the container was stopped with docker stop , but will restart if the docker daemon is restarted
```bash
docker run --restart unless-stopped {{image_name}}
```