# go commands

```bash
go help {{command}} # show details on a command
go bug # start a bug report
go build # compile packages and dependencies
go clean # remove object files and cached files
go doc # show documentation for package or symbol
go env # print GO environment information
go fix # update packages to use new APIs
go fmt # gofmt (reformat) package sources
go generate # generate Go files by processing source
go install # compile and install packages and dependencies
go list # list packages or modules
go mod # module maintenance 
go work # workspace maintenance 
go run # compile and run go program
go telementry # manage telementry data and settings
go test # test packages
go tool # run specified go tool
go version # print go version
go vet # report likely mistakes in packages
go tool {{pprof | cover | vet}} # runs a specified go tool 
```

## initialize 
```bash
go mod init {{module_name}}
go mod tidy # removes unused dependencies from go.mod and go.sum, and adds any missing dependencies
```

```bash
go get # add dependencies to current module and install them
go get {{package}}
go mod download # downloads the modules specified in go.mod to the local cache
go mod vendor
```