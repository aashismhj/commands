# MongoShell Commands

```bash
mongosh --host {{host}}
mongosh --tls --host {{host}} --tlsCAFile {{certificate_file}}.pem --username {{username}} --password {{password}}

```

mongosh --ssl --host docdb-2024-08-12-06-14-58.cluster-cvum48kwugdk.us-east-1.docdb.amazonaws.com:27017 --sslCAFile global-bundle.pem --username root --password rootPassword