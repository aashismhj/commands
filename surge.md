```bash
npm install -g surge # install surge
surge # publish with prompt
surge {{path}} {{domain}} # publish without prompts
surge encrypt {{domain}} # provision SSL cert for project
surge cert {{domain}} # view certs for project
surge config {{domain}} # view/change project configuration
surge list {{domain}} # list all project revisions
surge rollfore {{domain}} # change to next revision
surge rollback {{domain}} # change to previous revision
surge cutover {{domain}} # change to latest revision
surge discard {{revision}} # remove revision from system
surge files {{domain}} # list all project files
surge audit {{domain}} # audit edgenode state
surge bust {{domain}} # busts cache on all edgenodes
surge teardown {{domain}} # tear down a publish project
surge ssl {{domain}} {{pempath}} # legacy command for uploading pem file
surge invite {{domain}} {{emails}} # invites user to be a contributor
surge revoke {{domain}} {{email}} # revokes contributor rights
surge dns|zone {{domain}} # view DNS records
surge dns|zone {{domain}} add {{type}} {{name}} {{value}} # add dns records
surge dns|zone {{domain}} rem {{id}} # remove DNS record
surge list # list all projects
surge --version # outputs version
surge --help # output commands
```

## Analytics Commands
```bash
surge traffic {{domain}} # analytics showing project traffic
surge load {{domain}} # analytics showing global network load
surge audience {{domain}} # analytics showing audience device info
surge usage {{domain}} # analytics showing bandwidth usage
```

## Account Management
```bash
surge whoami # show who you are logged in as
surge login # only perform authentication step
surge logout # expire local token
surge token # create token for automation purposes
surge plan # upgrade or downgrade account plan
surge nuke # permanently removes account
```