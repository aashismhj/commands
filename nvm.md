```bash
#check version
node -v || node --version

# list installed versions of node (via nvm)
nvm ls
# OR
nvm list

# install a node version
nvm install <version>

# install latest version
nvm install --lts
# OR
nvm install node

# use latest
nvm use --lts
# OR
nvm use node

# set default node version
nvm alias default <version>
# set latest as default
nvm alias default node

# set user defined alias to node version
nvm alias <alias_name> <node_version>
# remove alias
nvm unalias <alias_name>

# list all node versions
nvm ls-remote
# listing the latest version
nvm ls-remote | grep -i 'latest'
# listing node version after a certain version
nvm ls-remote | grep -i 'v12'

# install latest npm release only
nvm install-latest-npm

# executing a file using a specific version
nvm run <version> <file_name>
# E.g
nvm run 6.10.3 app.js

# Run node app.js with the PATH pointing to node version
nvm exec <version> node <file_name>
# E.g
nvm exec 4.8.3 node app.js
# Set text colors to cyan, green, bold yellow, magenta, and white
nvm set-colors cgYmW

## uninstall
# uninstall node version
nvm uninstall <version>
# uninstall latest
nvm uninstall --lts
# uninstall latest (current ) release of node
nvm uninstall node
```