# Prisma Commands

```bash
prisma generate
prisma migrate dev --name init
prima migrate dev reset
prisma migrate dev --create-only
```

## Migrate Commands
### development
```bash
## Command for development
prisma migrate dev ## create a migration from changes in schema, also apply it to prisma client
prisma migrate dev --name migration-name --create-only
prisma migrate reset ## reset database and apply all migration
```

### production
```bash
prisma migrate deploy ## apply pending migrations to the database
prisma migrate status ## check the status of our database migration
prisma migrate status --schema={{path_to_schema}}
prisma migrate resolve ## resolve issues with the database migrations, i.e baseline failed migration, hotfix
```

### other migration commands
```bash
prisma migrate diff --from-url {{$DATABASE_URL}} --to-url {{url}} --script ## compare the database schema from two arbitrary sources
```