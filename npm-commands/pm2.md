# Pm2 commands

## General Commands
```bash
pm2 start {{file_name}} # start an application
pm2 start {{file_name}} # start an application with custom name
pm2 list # list applications
pm2 restart {{app_name}} # restart an application
pm2 stop {{app_name}} # stop application
pm2 delete {{app_name}} # delete application
pm2 logs {{app_name}} # view app logs
pm2 monit {{app_name}} # monitor application
pm2 start {{file_name}} -i max # start application in cluster mode
```

## Application Management
```bash
pm2 reload {{app_name}}
pm2 scale {{app_name}} {{number}} # scale the application instance (only works in cluster mode)
pm2 describe {{app_name}} # show detailed information about a specific process
pms2 flush # clear all logs for all applications managed by pm2
```

## Auto Start on Server Boot
```bash
pm2 startup # generate and configure a startup script to keep apps running after server reboot
pm2 save # save the list of currently running application
pm2 resurrect # reload previously saved applications
```

## Others
```bash
pm2 env {{app_id}} # view environment variables for a specific app by its ID
pm2 logs {{app_name}} --err # show only error logs for an application
pm2 kill # stop and delete all pm2 processes
pm2 update # update pm2 to the latest version
```