# Netlify Commands
https://docs.netlify.com/cli/get-started/

## login and auth
```bash
# connect current repo with netlify hosted site
netlify link
```

## Run builds locally
```bash
netlify build
# dry run
netlify build --dry
# deploy context
netlify build --context deploy-preview
```