# Venv Commands
```bash
python -m venv .venv # init virtual env
source .venv/Source/activate # activate virtual environment
source .venv/bin/activate # activate virtual environment on linux
deactivate # exit from environment
```