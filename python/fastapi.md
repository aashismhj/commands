# Fast Api commands

## fastapi
```bash
fastapi dev main.py # run in development mode
fastapi run # run server in prod
```

## uvicorn
```bash
uvicorn main:app --reload # 
uvicorn main:app --host 0.0.0.0 --port {{port}}

```

## gunicorn 
helps efficiently manage multiple worker processes to handle concurrent request, improving scalability and performance
```bash
gunicorn -w 4 app:app # sync workers handle one request per worker at a time, blocking until the request is complete
gunicorn -w 4 -k gthread app:app # async workers: handle multiple requests concurrently without blocking, useful for i/o bound tasks
gunicorn -w 4 -k unicorn.workers.UvicornWorker app:app # uvicorn worker, used for running ASGi (Asynchronous Server Gateway Interface) apps like FastAPI and Starlette. This is a worker class from Uvicorn that integrates with Gunicorn to serve ASGI apps, enabling asynchronous request handling
```