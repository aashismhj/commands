# Pip commands
```bash
pip --version
pip install -r requirements.txt
pip install -r {{destination}}/requirements.txt -t {{destination}}
pip list
pip freeze
pipenv lock -r
```