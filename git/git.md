# Git Commands

## Basic Commands
```bash
## Initialize repo
git init
git init --initial-branch=main ## initialize with a default branch name
## adding to staging
git add . ## all files in current dir and sub dir
## add a specific file
git add <file_name/>
## add all changes
git add -a

## commit
git commit -m "commit message"
## add and commit 
git commit -am "commit and add"

## soft reset commit
git reset --soft HEAD~<number/>

## hard reset 
git reset --hard HEAD~<number/>
```

## Branching
```bash
## list branch
git branch 
git branch -r ## list remote branch
git branch -b <new_branch_name/> ## new branch name
git checkout <branch_name/> ## checkout a branch
git checkout - ## checkout previous branch
git checkout -b <new_branch_name/> ## create and checkout branch
git branch -d <delete_branch_name/> ## Delete branch name
git branch -D <branch_name/> ## Force Delete branch
```

## Merge
```bash
git merge <merge_branch_name/>
git rebase <branch_name/> ## rebase a branch
```

```bash
git push --set-upstream origin main
```