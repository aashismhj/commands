# push a folder to new repo
Two ways: subtree Or filter-branch
```bash
cd /path/to/app
git remote add {{another_origin}} {{url}}
git subtree split --prefix={{/path/to/app}} -b {{branch_name}} # split the folder into its own branch
git checkout {{branch_name}}
git push {{another_origin}} {{branch_name}}:main
```