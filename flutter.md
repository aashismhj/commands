# Flutter Commands

## Basic Commands
```bash
flutter create {{project_name}} # create project
flutter run  # run flutter app
flutter build apk # build flutter app
flutter pub get # install dependencies
flutter upgrade # update flutter and dependencies
flutter analyze # analyze code for errors
flutter format . # format dart code files in the project 
flutter pub outdated # displays outdated packages and their versions
flutter clean # removes the build/directory and clean up intermediate files
```

## Device management commands
```bash
flutter devices # list all connected devices
flutter emulators # list all available emulators
flutter emulators --launch {{emulator_id}} # launch emulator by id

```

## Debuggin and Testing Commands
```bash
flutter run --release # runs flutter app in release mode, optimized for performance
flutter run --profile # run in profile mode for performance profiling
flutter test # runs all tests in the test/ directory
```